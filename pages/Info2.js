import React, { Component } from 'react';
import { MuiThemeProvider } from 'material-ui/styles';

import Header from '../components/Header';
import header from '../common/header';


export default class extends Component {

  render() {
    return (
      <MuiThemeProvider>
        <div>
          <Header
            title={header.appTitle}
            pages={header.pages}
          />

          Show cards
        </div>
      </MuiThemeProvider>
    );
  }
}
