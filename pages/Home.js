import React, { Component } from 'react';
import { AppBar, Drawer, MenuItem } from 'material-ui';
import Link from 'next/link';

import Header from '../components/Header';
import header from '../common/header';


export default class extends Component {
  constructor(props) {
    super(props);

    this.state = { open: false };
  }

  handleLeftIconTouch = () => {
    this.setState({ open: !this.state.open });
  }

  render() {

    return (
      <div>
        <Header
          title={header.appTitle}
          pages={header.pages}
        />
      </div>
    );
  }
}
