import React, { Component } from 'react';
import { getMuiTheme, MuiThemeProvider } from 'material-ui/styles';
import {
  Table, TableBody, TableFooter,
  TableHeader, TableHeaderColumn,
  TableRow, TableRowColumn
} from 'material-ui/Table';

import Header from '../components/Header';
import data from '../common/data';
import header from '../common/header';


const theme = getMuiTheme({}, {userAgent: false});

export default class extends Component {

  render() {
    const tableHeader = () => (
        <TableHeader>
          <TableRow>
            {
              Object.keys(data[0]).map((v, index) =>
                <TableHeaderColumn key={index}>{v}</TableHeaderColumn>
              )
            }
          </TableRow>
        </TableHeader>
    );

    const htmlNewLine = (string) => (
      string.includes('\n')
        ? string.split('\n')
                .map((line, index) => (
                  <span key={index}>
                    {line}<br />
                  </span>
                ))
        : string
    );

    const rowColumns = (row) => (
      Object.keys(row).map((key, index) => (
        <TableRowColumn key={index}>
          { htmlNewLine(row[key]) }
        </TableRowColumn>
      ))
    );

    const tableBody = () => (
      <TableBody>
        {
          data.map((row, index) => (
            <TableRow key={index}>
              { rowColumns(row) }
            </TableRow>
          ))
        }
      </TableBody>
    );

    return (
      <MuiThemeProvider muiTheme={theme}>
        <div>
          <Header
            title={header.appTitle}
            pages={header.pages}
          />
          <Table>
            { tableHeader() }
            { tableBody() }
          </Table>
        </div>
      </MuiThemeProvider>
    );
  }
}
