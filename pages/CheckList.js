import React, { Component } from 'react';
import { getMuiTheme, MuiThemeProvider } from 'material-ui/styles';
import {
  Table, TableBody, TableFooter,
  TableHeader, TableHeaderColumn,
  TableRow, TableRowColumn
} from 'material-ui/Table';
import { cyan700, grey100 } from 'material-ui/styles/colors';

import Header from '../components/Header';
import header from '../common/header';
import data from '../common/data2';


const theme = getMuiTheme({}, {userAgent: false});
const styles = {
  table: {
    header: {
      fontSize: '18px',
    },
  }
};

export default class extends Component {
  render() {
    return (
      <MuiThemeProvider muiTheme={theme}>
        <div>
          <Header
            title={header.appTitle + ' - 廠商檢核表'}
            pages={header.pages}
          />
          <Table>
            { tableHeader() }
            { tableBody() }
          </Table>
        </div>
      </MuiThemeProvider>
    );
  }
}

function tableHeader() {
  const checkHeaders = data[1].checkItems
    .map((row) => row.title);

  const companies = (
    <span>
      <i className="fa fa-id-badge" aria-hidden="true" />
      {" "}
      廠商名稱
    </span>
  );

  const headers = [companies, ...checkHeaders];

  return (
    <TableHeader style={styles.table.header}>
      <TableRow>
        {
          headers.map((h, index) =>
              <TableHeaderColumn
                key={index}
                style={styles.table.header}
              >
                {h}
              </TableHeaderColumn>
          )
        }
      </TableRow>
    </TableHeader>
  );
}

function tableBody(state) {
  const tableRow = (row, index) => {
    const { company, checkItems } = row;
    // const items = checkItems.map((e) => `${e.rate}\n備註：${e.comment}`);
    const flatRow = [company, ...checkItems];
    // if (row.checkItems !== null) {  // 如果是 `檢查項目/廠商名稱` 的標頭就忽略
      return (
        <TableRow
          key={index}
        >
          { rowColumns(flatRow) }
        </TableRow>
      );
    // }
  };

  return (
    <TableBody>
      { data.map(tableRow) }
    </TableBody>
  );
}

function rowColumns(row) {
  return row.map((item, index) => (
    <TableRowColumn key={index}>
      { decorateColumn(item) }
    </TableRowColumn>
  ))
}

function decorateColumn(item) {
  let result;

  switch (typeof item) {
    case 'object':
      const rate = rateToIcon(item.rate)
      result = (
        <span>
          { rate }<br />
          { `備註： ${item.comment}` }
        </span>
      );
      break;  // 一定要加 break，否則就是把 switch-case 換成 if-else

    case 'string':
      result = htmlNewLine(item);
      break;

    default:
      console.error('decorateColumn(): can not handle such item type');
  }

  return result;
}

function htmlNewLine(string) {
  const show = (line, index) => (
    <span key={index}>
      {line}<br />
    </span>
  );

  return (
    string.includes('\n')
      ? string.split('\n')
              .map(show)
      : string
  );
}

function rateToIcon(rate) {
  if (typeof rate !== 'number') {
    throw new Error('Argument [rate] must be typeof [Number]');
  }

  if (rate < 1 || rate > 5) {
    console.error('Rate should between 1 ~ 5');
  }

  if (rate >= 1 && rate < 3) {
    return <i className="fa fa-frown-o fa-2x" aria-hidden="true" />
  } else if (rate === 3) {
    return <i className="fa fa-meh-o fa-2x" aria-hidden="true" />
  } else {
    return <i className="fa fa-smile-o fa-2x" aria-hidden="true" />
  }
}
