import React, { Component } from 'react';
import { getMuiTheme, MuiThemeProvider } from 'material-ui/styles';
import Head from 'next/head';

import injectTapEventPlugin from 'react-tap-event-plugin';
// if (typeof window !== 'undefined') injectTapEventPlugin();
try {
  injectTapEventPlugin();
} catch (err) {
  console.error(err);
}

import Home from './Home';


export default class extends Component {

  render() {
    const theme = getMuiTheme({}, {userAgent: false});

    return (
      <div>
        <Head>
          <title>Play with MUI</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i" rel="stylesheet" />
          <link href="https://fonts.googleapis.com/css?family=Material+Icons" rel="stylesheet" />

          <link rel="icon" href="/static/favicon.ico" />
          <link href="/static/styles.css" rel="stylesheet" />
          <script src="https://use.fontawesome.com/1a003ca8a6.js"></script>
        </Head>

        <MuiThemeProvider muiTheme={theme}>
          <Home />
        </MuiThemeProvider>
      </div>
    );
  }
}
