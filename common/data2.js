const jazzRestaurant = {
	company: 'Jazz 餐廳',
	checkItems: [
    {
      title: '食物是否有乾淨',
      rate: 4,
      comment: '還可以',
    },
    {
      title: '食物是否符合學生胃口',
      rate: 2,
      comment: '不錯吃',
    },
    {
      title: '食物是否營養',
      rate: 4,
      comment: '蔬菜太少',
    },
	]
};

const jazzCafe = {
  company: 'Jazz 咖啡',
  checkItems: [
    {
      title: '食物是否有乾淨',
      rate: 4,
      comment: '應該乾淨吧',
    },
    {
      title: '食物是否符合學生胃口',
      rate: 2,
      comment: '還 OK 啦！',
    },
    {
      title: '食物是否營養',
      rate: 3,
      comment: '沒啥有營養的東西...',
    },
  ]
};

const soLaSo = {
  company: '手拉手漢堡',
  checkItems: [
    {
      title: '食物是否有乾淨',
      rate: 4,
      comment: '尚可',
    },
    {
      title: '食物是否符合學生胃口',
      rate: 5,
      comment: '邱好吃',
    },
    {
      title: '食物是否營養',
      rate: 1,
      comment: '朝向米其林之路的必備食物',
    },
  ]
};


export default [
  jazzRestaurant,
  jazzCafe,
  soLaSo,
];
