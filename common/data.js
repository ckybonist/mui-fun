export default [
  {
    name: "KuanYu",
    sid: "10425001",
    dept: "Applied Informatics",
    remark: "Need some workout.",
  },
  {
    name: "Kiki",
    sid: "10425002",
    dept: "Applied Informatics",
    remark: "I am a cat who lives in DCLab.\nTease me for fun!",
  },
  ,
  {
    name: "Wiwi",
    sid: "10425003",
    dept: "Applied Informatics",
    remark: "I am a cat who lives with kiki.\nI am hungry.\nI had fallen from 4th floor.",
  },
];
