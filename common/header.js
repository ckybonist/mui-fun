export default {
  appTitle: "KikiYo",
  pages: [
    {
      name: "Home",
      href: "/",
    },
    {
      name: "Student Table",
      href: "/Info1",
    },
    {
      name: "Student Cards",
      href: "/Info2",
    },
    {
      name: "Check List",
      href: "/CheckList",
    },
  ],
}
