import React, { Component } from 'react';
import { AppBar, Drawer, MenuItem } from 'material-ui';
import Link from 'next/link';


export default class extends Component {
  constructor(props) {
    super(props);

    this.state = { open: false };
  }

  handleLeftIconTouch = () => {
    this.setState({ open: !this.state.open });
  }

  render() {
    const { title, pages } = this.props;

    return (
      <div>
        <AppBar
          title={title}
          onLeftIconButtonTouchTap={this.handleLeftIconTouch}
        />

        <Drawer
          docked={false}
          open={this.state.open}
          onRequestChange={(open) => this.setState({ open })}
        >
          {
            pages.map((page, index) => (
              <MenuItem key={index}>
                <Link href={page.href}>
                  <a>{page.name}</a>
                </Link>
              </MenuItem>
            ))
          }
        </Drawer>
      </div>
    );
  }
}
